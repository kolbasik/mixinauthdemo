﻿<%@ Page Title="Log in" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MixinAuthDemo.Web.Login" %>

<!DOCTYPE html>
<html>
<head runat="server">
	<title><%= this.Page.Title %></title>
</head>
<body>
	<form runat="server">
		<div>
			<asp:Label runat="server" ID="UserNameLabel">User name</asp:Label>
			<asp:TextBox runat="server" ID="UserNameTextBox" />
			<asp:Button runat="server" ID="SubmitButton" Text="Log in" OnClick="SubmitButton_Click" />
		</div>
	</form>
</body>
</html>
