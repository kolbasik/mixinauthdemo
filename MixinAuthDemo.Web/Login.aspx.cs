﻿using System;
using System.Web.Security;
using System.Web.UI;

namespace MixinAuthDemo.Web
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            FormsAuthentication.RedirectFromLoginPage(UserName, createPersistentCookie: false);
        }

        public string UserName
        {
            get { return this.UserNameTextBox.Text; }
        }
    }
}