﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Web.Security;

namespace MixinAuthDemo.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            var context = this.Context;
            var windowsIdentity = context.Request.LogonUserIdentity;
            if (windowsIdentity != null)
            {
                if (context.Request.Url.AbsolutePath.EndsWith(FormsAuthentication.LoginUrl))
                {
                    // NOTE: make the fake authorization using the real windows credential
                    FormsAuthentication.RedirectFromLoginPage(windowsIdentity.Name, false);
                }
                else if (string.Equals(windowsIdentity.Name, context.User.Identity.Name))
                {
                    // NOTE: restore the real windows credential overriding the form credential
                    Thread.CurrentPrincipal = context.User = new WindowsPrincipal(windowsIdentity);
                    // NOTE: identity impersonalization
                    context.Items["WindowsImpersonationContext"] = windowsIdentity.Impersonate();
                }
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var windowsImpersonationContext = this.Context.Items["WindowsImpersonationContext"] as IDisposable;
            if (windowsImpersonationContext != null)
                windowsImpersonationContext.Dispose();
        }
    }
}