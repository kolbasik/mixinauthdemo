﻿using System;
using System.Data.Entity;
using System.Linq;

namespace MixinAuthDemo.Web
{
    public partial class Default : System.Web.UI.Page
    {
        public SqlUserInfoDto SqlUserInfo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var demoDataContext = new DemoDataContext();
            demoDataContext.Database.CreateIfNotExists();
            SqlUserInfo = demoDataContext.Database.SqlQuery<SqlUserInfoDto>("select SYSTEM_USER as UserName").First();
        }

        public class DemoDataContext : DbContext
        {
            public DemoDataContext() : base("name=DemoConnection")
            {
            }
        }

        public class SqlUserInfoDto
        {
            public string UserName { get; set; }
        }
    }
}