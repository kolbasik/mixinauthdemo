﻿<%@ Page Title="Mixin Auth: Windows and Forms" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MixinAuthDemo.Web.Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%= this.Page.Title %></title>
</head>
<body>
    <div>UserName: <%= this.Context.User.Identity.Name %></div>
    <div>SqlUserName: <%= this.SqlUserInfo.UserName %></div>
</body>
</html>
